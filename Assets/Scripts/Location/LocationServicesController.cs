using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocationServicesController : Singleton<LocationServicesController>
{
    public int updateTime = 5;
    [HideInInspector] public bool isSearching = false;

    [HideInInspector] public float latitude;
    [HideInInspector] public float longitude;

    [HideInInspector] public System.Action onLocationServiceCompleted;

    public void GetLocation()
    {
        if (isSearching == false)
        {
            isSearching = true;
            StartCoroutine(LocationServiceUpdate());
        }
    }

    IEnumerator LocationServiceUpdate()
    {
        Input.location.Start();

        int currentWait = updateTime;

        while (Input.location.status == LocationServiceStatus.Initializing && currentWait > 0)
        {
            yield return new WaitForSeconds(1);
            currentWait--;
        }
        Debug.Log("location data get");
        if (updateTime <= 0)
        {
            Debug.Log("location service timed out");
            isSearching = false;
            yield break;
        }

        if (Input.location.status == LocationServiceStatus.Failed || Input.location.status == LocationServiceStatus.Stopped)
        {
            Debug.Log("Failed to determine device location");
            isSearching = false;
            yield break;
        }

        latitude = Input.location.lastData.latitude;
        longitude = Input.location.lastData.longitude;

        Input.location.Stop();
        isSearching = false;

        onLocationServiceCompleted();
        
    }
}
