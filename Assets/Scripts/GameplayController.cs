using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class GameplayController : Singleton<GameplayController>
{
    public Text messageText;
    public Button catchBtn;
    public Image iconImage;
    public Text nameText;
    public InputField indexInput;
    public Text locationText;

    private bool isAssetReady = false;

    [System.Serializable]
    public class PokemonInfo
    {
        public int id;
        public string name;
        public PokemonSprite sprites;
    }

    [System.Serializable]
    public class PokemonSprite
    {
        public string front_default;
    }

    // Start is called before the first frame update
    void Start()
    {
        catchBtn.interactable = false;
        iconImage.enabled = false;
        PlayfabManager.Instance.LoginStateChanged.AddListener(LoginStateChanged);
        indexInput.onValueChanged.AddListener(OnIndexValueChanged);

        LocationServicesController.Instance.onLocationServiceCompleted += OnLocationServiceCompleted;
        
    }

    private void OnDestroy()
    {
        LocationServicesController.Instance.onLocationServiceCompleted -= OnLocationServiceCompleted;
    }

    private void OnLocationServiceCompleted()
    {
        locationText.text = String.Format("Hello, {0} in Lat: {1} Lon:{2}", PlayfabManager.Instance.playerInfo.name, LocationServicesController.Instance.latitude, LocationServicesController.Instance.longitude);
    }

    public PokemonInfo GetPokemon(int _index)
    {
        string url = String.Format("https://pokeapi.co/api/v2/pokemon/{0}", _index);
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        StreamReader reader = new StreamReader(response.GetResponseStream());
        string jsonResponse = reader.ReadToEnd();

        return JsonUtility.FromJson<PokemonInfo>(jsonResponse);
    }

    public void UpdatePokemonInfo(PokemonInfo _pokemon)
    {
        isAssetReady = false;
        if (_pokemon != null && _pokemon.sprites.front_default != null)
        {
            nameText.text = _pokemon.name;
            messageText.text = "Wild " + _pokemon.name + " appears!";
            StartCoroutine(GetImage(_pokemon.sprites.front_default));
        }
        else
        {
            Debug.LogWarning("Error with getting icon.");
        }
    }

    IEnumerator GetImage(string _url)
    {
        UnityWebRequest request = UnityWebRequest.Get(_url);
        yield return request.SendWebRequest();

        if (request.result != UnityWebRequest.Result.Success)
        {
            Debug.Log(request.error);
        }
        else
        {
            isAssetReady = true;
            iconImage.enabled = true;
            Texture2D texture = new Texture2D(2, 2);
            texture.LoadImage(request.downloadHandler.data);
            iconImage.sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
        }
    }

    public void OnIndexValueChanged(string _string)
    {
        int index = -1;
        if (_string == null || int.TryParse(_string, out index) == false || index <= 0)
        {
            Debug.Log("Invalid number");
            return;
        }
        else
        {
            UpdatePokemonInfo(GetPokemon(index));
        }

    }

    public void LoginStateChanged(PlayfabManager.LoginState _state)
    {
        switch (_state)
        {
            case PlayfabManager.LoginState.Wait:
                catchBtn.interactable = false;
                
                break;
            case PlayfabManager.LoginState.Success:
                catchBtn.interactable = false;
                break;
            case PlayfabManager.LoginState.GotProfile:
                catchBtn.interactable = true;
                locationText.text = "Hello, " + PlayfabManager.Instance.playerInfo.name;
                LocationServicesController.Instance.GetLocation();
                break;
            default:
                break;
        }
    }
}
