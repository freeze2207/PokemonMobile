using PlayFab;
using PlayFab.ClientModels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PlayfabManager : Singleton<PlayfabManager>
{
    public enum LoginState
    {
        Startup,
        Wait,
        Success,
        GotProfile,
        Failed
    }

    [System.Serializable]
    public class PlayerInfo
    {
        public string playerID;
        public string PlayfabID;
        public string name;
        public int totalCatch;
    }

    [System.Serializable]
    public class GameStateChangedEvent : UnityEvent<PlayfabManager.LoginState> { }

    // Runtime property

    public LoginState state = LoginState.Startup;
    [HideInInspector] public GameStateChangedEvent LoginStateChanged;
    [HideInInspector] public string PlayerID = "";
    private float currentDelay = 0.0f;
    private List<GameObject> leaderboardList;
    [HideInInspector] public PlayerInfo playerInfo;

    // Editable property
    public bool createNewPlayer = false;
    public string defaultPlayerName = "Unknown Player";
    public InputField UserNameInput;
    public Button UpdateUserNameButton;
    [SerializeField] private float updateTimeDelay = 5.0f;
    public GameObject leaderBoardItem;
    public GameObject leaderBoardContainer;


    private void Awake()
    {
        PlayerID = PlayerPrefs.GetString("PlayerID", "");
        if (string.IsNullOrEmpty(PlayerID) || createNewPlayer)
        {
            PlayerID = System.Guid.NewGuid().ToString();
            PlayerPrefs.SetString("PlayerID", PlayerID);
        }
        playerInfo.playerID = PlayerID;
    }

    private void Start()
    {
        UserNameInput.enabled = false;
        UpdateUserNameButton.interactable = false;

        leaderboardList = new List<GameObject>();

        LoginWithCustomIDRequest request = new LoginWithCustomIDRequest { CustomId = PlayerID, CreateAccount = true };
        PlayFabClientAPI.LoginWithCustomID(request, OnLoginSuccess, OnLoginFailure);
    }

    // Auto-update leader board 
    private void Update()
    {
        this.currentDelay += Time.deltaTime;
        if (this.currentDelay > this.updateTimeDelay)
        {
            if (state == LoginState.Success || state == LoginState.GotProfile)
            {
                RefreshLeaderboard();
            }
            this.currentDelay = 0.0f;
        }
    }

    // Logged in then get player profile
    private void OnLoginSuccess(LoginResult result)
    {
        ChangeLoginState(LoginState.Success);
        Debug.Log($"Congratulations, you have logged into Playfab!");

        UpdatePlayerProfile(result.PlayFabId);
    }

    // Loggin and get player profile failed
    private void OnLoginFailure(PlayFabError error)
    {
        ChangeLoginState(LoginState.Failed);
        Debug.LogWarning($"Something went wrong loggined into Playfab / getting player profile.");
        Debug.LogWarning($"Here 's some debug info " + error.GenerateErrorReport());
    }

    // Update player profile with playerfab ID
    public void UpdatePlayerProfile(string _playerFabId)
    {
        GetPlayerProfileRequest profileRequest = new GetPlayerProfileRequest { PlayFabId = _playerFabId, ProfileConstraints = new PlayerProfileViewConstraints { ShowDisplayName = true, ShowStatistics = true } };
        PlayFabClientAPI.GetPlayerProfile(profileRequest, OnGetPlayerSuccess, OnLoginFailure);
    }
    
    // Successfully update player profile
    private void OnGetPlayerSuccess(GetPlayerProfileResult result)
    {
        if (result.PlayerProfile.DisplayName == null)
        {
            UpdateUserName(defaultPlayerName);
        }

        UserNameInput.enabled = true;
        UpdateUserNameButton.interactable = true;

        this.playerInfo.PlayfabID = result.PlayerProfile.PlayerId;
        this.playerInfo.name = result.PlayerProfile.DisplayName;

        // player has record 
        if (result.PlayerProfile.Statistics != null && result.PlayerProfile.Statistics.Count > 0)
        {
            foreach (StatisticModel item in result.PlayerProfile.Statistics)
            {
                if (item.Name == "total_catch")
                {
                    this.playerInfo.totalCatch = item.Value;
                }
            }
        }
        // init player record if not
        else
        {
            PlayFabClientAPI.UpdatePlayerStatistics(new UpdatePlayerStatisticsRequest
            {
                Statistics = new List<StatisticUpdate>
                {
                    new StatisticUpdate { StatisticName = "total_catch", Value = 0 }
                }
            },
            OnUpdatePlayerStatistics, 
            OnPlayfabError
            );
            this.playerInfo.totalCatch = 0;
        }

        UserNameInput.text = this.playerInfo.name;
        ChangeLoginState(LoginState.GotProfile);
        RefreshLeaderboard();
        GameplayController.Instance.UpdatePokemonInfo(GameplayController.Instance.GetPokemon(Random.Range(1, 100)));
        Debug.Log($"Hello, {this.playerInfo.name}!");
        Debug.Log($"You have caught {this.playerInfo.totalCatch} Pokemon.");
    }

    private void OnUpdatePlayerStatistics(UpdatePlayerStatisticsResult result)
    {
        Debug.Log("User stats updated");
        ChangeLoginState(LoginState.GotProfile);
    }

    private void OnPlayfabError(PlayFabError error)
    {
        Debug.Log("Error on stat update/get");
    }

    public void OnClickChangeUserName()
    {
        UpdateUserName(UserNameInput.text);
        ChangeLoginState(LoginState.Wait);
    }

    private void UpdateUserName(string _name)
    {
        PlayFabClientAPI.UpdateUserTitleDisplayName(new UpdateUserTitleDisplayNameRequest() { DisplayName = _name },
            result => {
                this.playerInfo.name = _name;
                ChangeLoginState(LoginState.GotProfile);
                Debug.Log("Username changed successfully");
            },
            error =>
            {
                Debug.Log("Failed to change the user name");
            });
    }

    private void RefreshLeaderboard()
    {
        
        PlayFabClientAPI.GetLeaderboard(
            new GetLeaderboardRequest
            {
                StatisticName = "total_catch",
                StartPosition = 0,
                MaxResultsCount = 50
            },
            OnGetleaderboard,
            OnPlayfabError
            );
    }

    private void OnGetleaderboard(GetLeaderboardResult result)
    {
        string boardName = (result.Request as GetLeaderboardRequest).StatisticName;
        Debug.Log($"Get Leaderboard success: {boardName}");

        foreach (GameObject go in leaderboardList)
        {
            Destroy(go);
        }
        leaderboardList.Clear();

        foreach (PlayerLeaderboardEntry playerDetails in result.Leaderboard)
        {
            GameObject item = Instantiate(leaderBoardItem, leaderBoardContainer.transform);
            item.GetComponent<Text>().text = string.Format("{0}, {1} total catch: {2}", playerDetails.Position + 1, playerDetails.DisplayName, playerDetails.StatValue);
            item.SetActive(true);
            this.leaderboardList.Add(item);
        }
    }

    public void OnClickCatch()
    {
        ChangeLoginState(LoginState.Wait);
        this.playerInfo.totalCatch++;
        UpdateTotalCatch(this.playerInfo.totalCatch);
        RefreshLeaderboard();
        GameplayController.Instance.UpdatePokemonInfo(GameplayController.Instance.GetPokemon(Random.Range(1, 100)));
    }

    private void UpdateTotalCatch(int _number)
    {
        PlayFabClientAPI.UpdatePlayerStatistics(new UpdatePlayerStatisticsRequest
        {
            Statistics = new List<StatisticUpdate>
            {
                new StatisticUpdate { StatisticName = "total_catch", Value = _number }
            }
        },
        OnUpdatePlayerStatistics, 
        OnPlayfabError
        );
    }

    public void ChangeLoginState(LoginState _state)
    {
        state = _state;
        switch (state)
        {
            case LoginState.Startup:
                break;
            case LoginState.Wait:
                break;
            case LoginState.Success:
                break;
            case LoginState.GotProfile:
                break;
            case LoginState.Failed:
                break;
            default:
                break;
        }
        LoginStateChanged.Invoke(state);
    }
}
